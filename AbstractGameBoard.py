
class AbstractGameBoard( object ):
	def __init__( self, gamestate ):
		pass

	def GetRelativePixel( self, start_pos, facing_dir,
		offset_fwd, offset_right ):
		"""Given a position to stand in the arena, and a direction to face,
		return the status (0 for empty, >0 for non-empty) of a pixel that
		is offset_fwd pixels in front, and offset_right pixels to the right
		(negative values may be used to go backwards or left respectively).
		Pixels outside the arena also return >0."""
		pass

	def GetAbsolutePixel( self, (x, y) ):
		"""Return 0 if the absolute position specified does not contain
		a pixel, and >0 if it does.  Pixels outside the arena also
		return >0."""
		pass

	def GetArenaSize( self ):
		"""Return the size of the arena as a tuple (width, height)."""
		pass

	def GetPlayerPositions( self, pos_to_exclude = None ):
		"""Returns a list of pairs (pos, dir) for each player on screen.
		Excludes the player at the position specified if pos_to_exclude
		is not None."""
		pass

	def TurnRight( self, direction ):
		"""Return the direction found by turning 90 degrees right from
		the supplied direction."""
		pass

	def TurnLeft( self, direction ):
		"""Return the direction found by turning 90 degrees left from
		the supplied direction."""
		pass

