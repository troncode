#!/usr/bin/python -u

# troncode - write programs to play the classic lines game
#
# Copyright (C) 2008 Andy Balaam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
# USA.
#

import gc
import math
import os
import pygame
from pygame.locals import *
import random
import re
import sys
import time

from BasicGameBoard import BasicGameBoard
from mopelib import mopelib
from troncode_values import *

# ----------------------
class TronCodeConfig( mopelib.Config ):

	def default_config( self ):
		self.num_players = 2
		self.tick_time = 20

		self.screen_size = ( 610, 635 )
		self.colour_background = ( 0, 0, 0 )

		self.volume = 50

		self.music_on = 1
		self.sound_effects_on = 1

		self.keys_menu = mopelib.MyInputEvent( "Escape" )
		self.keys_menu.add( pygame.KEYDOWN, pygame.K_ESCAPE )
		self.keys_menu.add( pygame.JOYBUTTONDOWN, 8 )	# GP2X Start
		self.keys_menu.add( pygame.JOYBUTTONDOWN, 9 )	# GP2X Select

		self.keys_return = mopelib.MyInputEvent( "Return" )
		self.keys_return.add( pygame.KEYDOWN, pygame.K_RETURN )
		self.keys_return.add( pygame.JOYBUTTONDOWN, 13 )	# GP2X B button

		self.keys_startgame = mopelib.MyInputEvent( "any key" )
		self.keys_startgame.add_all( pygame.KEYDOWN )
		self.keys_startgame.add_all( pygame.JOYBUTTONDOWN )
		self.keys_startgame.add_all( pygame.MOUSEBUTTONDOWN )

		self.keys_up = mopelib.MyInputEvent( "up" )
		self.keys_up.add( pygame.KEYDOWN, ord( 'q' ) )
		self.keys_up.add( pygame.KEYDOWN, pygame.K_UP )
		self.keys_up.add( pygame.JOYBUTTONDOWN, 0 )		# GP2X Joy up
		self.keys_up.add( pygame.JOYBUTTONDOWN, 15 )	# GP2X Y button

		self.keys_right = mopelib.MyInputEvent( "right" )
		self.keys_right.add( pygame.KEYDOWN, ord( 'p' ) )
		self.keys_right.add( pygame.KEYDOWN, pygame.K_RIGHT )
		self.keys_right.add( pygame.JOYBUTTONDOWN, 6 )	# GP2X Joy right
		self.keys_right.add( pygame.JOYBUTTONDOWN, 13 )	# GP2X B button

		self.keys_down = mopelib.MyInputEvent( "down" )
		self.keys_down.add( pygame.KEYDOWN, ord( 'a' ) )
		self.keys_down.add( pygame.KEYDOWN, pygame.K_DOWN )
		self.keys_down.add( pygame.JOYBUTTONDOWN, 4 )	# GP2X Joy down
		self.keys_down.add( pygame.JOYBUTTONDOWN, 14 )	# GP2X X button

		self.keys_left = mopelib.MyInputEvent( "left" )
		self.keys_left.add( pygame.KEYDOWN, ord( 'o' ) )
		self.keys_left.add( pygame.KEYDOWN, pygame.K_LEFT )
		self.keys_left.add( pygame.JOYBUTTONDOWN, 2 )	# GP2X Joy left
		self.keys_left.add( pygame.JOYBUTTONDOWN, 12 )	# GP2X A button

		self.keys_volup = mopelib.MyInputEvent( "+" )
		self.keys_volup.add( pygame.KEYDOWN, ord( '+' ) )
		self.keys_volup.add( pygame.KEYDOWN, ord( '=' ) )
		self.keys_volup.add( pygame.JOYBUTTONDOWN, 16 )	# GP2X volume + button

		self.keys_slow = mopelib.MyInputEvent( "s" )
		self.keys_slow.add( pygame.KEYDOWN, ord( 's' ) )

		self.keys_fast = mopelib.MyInputEvent( "f" )
		self.keys_fast.add( pygame.KEYDOWN, ord( 'f' ) )

		self.keys_skip_to_end = mopelib.MyInputEvent( "k" )
		self.keys_skip_to_end.add( pygame.KEYDOWN, ord( 'k' ) )

		self.keys_voldown = mopelib.MyInputEvent( "-" )
		self.keys_voldown.add( pygame.KEYDOWN, ord( '-' ) )
		self.keys_voldown.add( pygame.JOYBUTTONDOWN, 17 ) # GP2X volume - button

		self.keys_p1_up = mopelib.MyInputEvent( "p1_up" )
		self.keys_p1_up.add( pygame.KEYDOWN, pygame.K_UP )
		self.keys_p1_up.add( pygame.JOYBUTTONDOWN, 0 )		# GP2X Joy up

		self.keys_p1_right = mopelib.MyInputEvent( "p1_right" )
		self.keys_p1_right.add( pygame.KEYDOWN, pygame.K_RIGHT )
		self.keys_p1_right.add( pygame.JOYBUTTONDOWN, 6 )		# GP2X Joy right

		self.keys_p1_down = mopelib.MyInputEvent( "p1_down" )
		self.keys_p1_down.add( pygame.KEYDOWN, pygame.K_DOWN )
		self.keys_p1_down.add( pygame.JOYBUTTONDOWN, 4 )		# GP2X Joy down

		self.keys_p1_left = mopelib.MyInputEvent( "p1_left" )
		self.keys_p1_left.add( pygame.KEYDOWN, pygame.K_LEFT )
		self.keys_p1_left.add( pygame.JOYBUTTONDOWN, 4 )		# GP2X Joy left

		self.keys_p2_up = mopelib.MyInputEvent( "p2_up" )
		self.keys_p2_up.add( pygame.KEYDOWN, ord( '2' ) )
		self.keys_p2_up.add( pygame.JOYBUTTONDOWN, 15 )	# GP2X Y button

		self.keys_p2_right = mopelib.MyInputEvent( "p2_right" )
		self.keys_p2_right.add( pygame.KEYDOWN, ord( 'e' ) )
		self.keys_p2_right.add( pygame.JOYBUTTONDOWN, 13 )	# GP2X B button

		self.keys_p2_down = mopelib.MyInputEvent( "p2_down" )
		self.keys_p2_down.add( pygame.KEYDOWN, ord( 'w' ) )
		self.keys_p2_down.add( pygame.JOYBUTTONDOWN, 14 )	# GP2X X button

		self.keys_p2_left = mopelib.MyInputEvent( "p2_left" )
		self.keys_p2_left.add( pygame.KEYDOWN, ord( 'q' ) )
		self.keys_p2_left.add( pygame.JOYBUTTONDOWN, 12 )	# GP2X A button

		self.keys_p3_up = mopelib.MyInputEvent( "p3_up" )
		self.keys_p3_up.add( pygame.KEYDOWN, ord( 'i' ) )

		self.keys_p3_right = mopelib.MyInputEvent( "p3_right" )
		self.keys_p3_right.add( pygame.KEYDOWN, ord( 'l' ) )

		self.keys_p3_down = mopelib.MyInputEvent( "p3_down" )
		self.keys_p3_down.add( pygame.KEYDOWN, ord( 'k' ) )

		self.keys_p3_left = mopelib.MyInputEvent( "p3_left" )
		self.keys_p3_left.add( pygame.KEYDOWN, ord( 'j' ) )

		self.keys_p4_up = mopelib.MyInputEvent( "p4_up" )
		self.keys_p4_up.add( pygame.KEYDOWN, pygame.K_KP8 )

		self.keys_p4_right = mopelib.MyInputEvent( "p4_right" )
		self.keys_p4_right.add( pygame.KEYDOWN, pygame.K_KP6 )

		self.keys_p4_down = mopelib.MyInputEvent( "p4_down" )
		self.keys_p4_down.add( pygame.KEYDOWN, pygame.K_KP5 )
		self.keys_p4_down.add( pygame.KEYDOWN, pygame.K_KP2 )

		self.keys_p4_left = mopelib.MyInputEvent( "p4_left" )
		self.keys_p4_left.add( pygame.KEYDOWN, pygame.K_KP4 )


# ----------------------

class TronCodeSoundManager( mopelib.SoundManager ):

	def __init__( self, volume ):
		mopelib.SoundManager.__init__( self, config )

		#self.add_sample_group( "waddles", ["waddle1"] )

# ----------------------

def intro_draw_keys():
	keys_colour = (0, 0, 0)
	write_text( "Keys", keys_colour, 0.2, 0.05 )
	write_text( "Slow down: %s" % config.keys_slow.name, keys_colour, 0.1, 0.25 )
	write_text( "Speed up: %s" % config.keys_fast.name, keys_colour, 0.1, 0.35 )
	write_text( "Skip round: %s" % config.keys_skip_to_end.name, keys_colour, 0.1, 0.45 )

# ----------------------

def intro_draw_instructions():
	write_text( "Press %s for menu, or %s to start" % (
		config.keys_menu.name, config.keys_startgame.name ),
		(0, 0, 0), 0.05, 0.99 )

# ----------------------

def general_menu_create_menu( menu, config, gamestate ):
	menu.items = []
	if gamestate == None:	# We are on a title screen - Start Game option
		menu.add_item( "Start game", MENU_START )
		menu.add_item( "Number of players: %d" % (config.num_players),
			MENU_NUM_PLAYERS )
		for player_num in range( config.num_players ):
			cls_name = "--unknown--"
			if player_num < len( config.player_classes ):
				cls_name = config.player_classes[player_num].GetName()
			menu.add_item( "P%d: %s" % ( player_num, cls_name ),
				MENU_CHANGE_PLAYER + player_num )
	else:
		menu.add_item( "Continue", MENU_START )
		menu.add_item( "End game", MENU_END )

	tmp_str = "Music: "
	if config.music_on:
		tmp_str += "on"
	else:
		tmp_str += "off"
	menu.add_item( tmp_str, MENU_MUSIC )

	tmp_str = "Effects: "
	if config.sound_effects_on:
		tmp_str += "on"
	else:
		tmp_str += "off"
	menu.add_item( tmp_str, MENU_SOUND_EFFECTS )

	menu.add_item( "Quit troncode", MENU_QUIT )

	return menu

def general_menu_screen( config, gamestate ):

	if gamestate == None:
		menu_title = "troncode"
	else:
		menu_title = "troncode paused"

	menu = mopelib.Menu()
	general_menu_create_menu( menu, config, gamestate )
	menurender.set_menu( menu, menu_title )
	menurender.repaint_full()

	game_start = False

	waiting = True
	while waiting:
		event = pygame.event.wait()
		if event.type == QUIT:
			sys.exit(0)
		elif config.keys_menu.matches( event ):
			waiting = False
		elif config.keys_down.matches( event ):
			menurender.move_down()
		elif config.keys_up.matches( event ):
			menurender.move_up()
		elif config.keys_return.matches( event ):
			code = menu.get_selected_item().code
			if code == MENU_START:
				game_start = True
				waiting = False
			elif code == MENU_END:
				gamestate.alive = INGAME_QUIT
				waiting = False
			elif code == MENU_MUSIC:
				if config.music_on == 1:
					config.music_on = 0
				else:
					config.music_on = 1
				general_menu_create_menu( menu, config, gamestate )
				menurender.repaint_full()
				sound_mgr.setup( gamestate )
				config.save()
			elif code == MENU_SOUND_EFFECTS:
				if config.sound_effects_on:
					config.sound_effects_on = 0
				else:
					config.sound_effects_on = 1
				general_menu_create_menu( menu, config, gamestate )
				menurender.repaint_full()
				sound_mgr.setup( gamestate )
				config.save()
			elif code == MENU_QUIT:
				sys.exit(0)

	return game_start

# ----------------------

def intro_draw_title():
	screen.blit( intro_surface_title, (0,0) )
	write_text( "Version " + troncode_version,
		( 0, 0, 0 ), 0.05, 0.88 )
	write_text( "by Andy Balaam", ( 0, 0, 0 ), 0.06, 0.93 )
	intro_draw_instructions()

def intro_draw_something( intro_mode ):
	if intro_mode == INTRO_MODE_TITLE:
		intro_draw_title()
	elif intro_mode == INTRO_MODE_INSTR:
		screen.blit( intro_surface_instr, (0,0) )
		intro_draw_instructions()
	elif intro_mode == INTRO_MODE_MUSIC:
		screen.blit( intro_surface_music, (0,0) )
		intro_draw_keys()
		intro_draw_instructions()
	pygame.display.update()

def intro_input( event, config, intro_mode ):
	if event.type == QUIT:
		sys.exit(0)
	elif config.keys_volup.matches( event ):
		config.volume = sound_mgr.increase_volume()
		config.save()
	elif config.keys_voldown.matches( event ):
		config.volume = sound_mgr.decrease_volume()
		config.save()
	else:
		if event.type == EVENTTYPE_TITLE_TICK:
			intro_mode += 1
			if intro_mode == INTRO_MODE_ENDED:
				intro_mode = INTRO_MODE_TITLE
			intro_draw_something( intro_mode )
		elif config.keys_menu.matches( event ):
			mopelib.clear_events( EVENTTYPE_TITLE_TICK )
			start_game = general_menu_screen( config, None )
			if start_game:
				intro_mode = INTRO_MODE_ENDED
			else:
				intro_draw_something( intro_mode )
			pygame.time.set_timer( EVENTTYPE_TITLE_TICK, TITLE_TICK_TIME )
		elif config.keys_startgame.matches( event ):
			intro_mode = INTRO_MODE_ENDED
	return intro_mode

# ----------------------

def intro_mainloop( config ):
	intro_draw_title()

	pygame.display.update()
	pygame.time.set_timer( EVENTTYPE_TITLE_TICK, TITLE_TICK_TIME )

	intro_mode = INTRO_MODE_TITLE
	while intro_mode < INTRO_MODE_ENDED:
		intro_mode = intro_input( pygame.event.wait(), config, intro_mode )

	mopelib.clear_events( EVENTTYPE_TITLE_TICK )

def draw_pixel( gamestate, surface, colour, x, y ):
	if colour in gamestate.alive_colours:
		col = colour
	else:
		col = mopelib.dim_colour( colour, gamestate.dim )
	adj_x = screen_border[0] + scale * x
	adj_y = screen_border[1] + scale * y
	if scale < 1:
		surface.set_at( ( int(adj_x), int(adj_y) ), col )
	else:
		pygame.draw.rect( surface, col,
			(adj_x, adj_y, ceil_scale, ceil_scale ) )

def inlevel_screen_blit( gamestate ):
	screen.blit( gamestate.offscreen_buff, (0,0) )
	pygame.display.update()

def inlevel_redraw_screen( gamestate, arrows, scores ):
	gamestate.offscreen_buff = pygame.Surface( gamestate.config.screen_size )
	gamestate.offscreen_buff.blit( ingame_surface_background, (0,0) )
	for x, y, colour in gamestate.pixels_list:
		draw_pixel( gamestate, gamestate.offscreen_buff, colour, x, y )
	inlevel_draw_players( gamestate )
	write_text_ingame( gamestate, scores )
	# TODO: draw arrows
	inlevel_screen_blit( gamestate )

def inlevel_draw_players( gamestate ):
	for player in gamestate.players:
		x, y = gamestate.GetPosition( player )
		draw_pixel( gamestate, gamestate.offscreen_buff, player.GetColour(),
			x, y )

def inlevel_update_screen( gamestate, scores ):
	inlevel_draw_players( gamestate )
	inlevel_screen_blit( gamestate )



# ----------------------

def inlevel_input( event, gamestate, scores ):
	if event.type == QUIT:
		sys.exit(0)
	elif config.keys_menu.matches( event ):
		general_menu_screen( config, gamestate )
		inlevel_redraw_screen( gamestate, False, scores )
	elif config.keys_volup.matches( event ):
		config.volume = sound_mgr.increase_volume()
		config.save()
	elif config.keys_voldown.matches( event ):
		config.volume = sound_mgr.decrease_volume()
		config.save()
	elif config.keys_slow.matches( event ):
		gamestate.framerate = 0
		inlevel_redraw_screen( gamestate, False, scores )
	elif config.keys_fast.matches( event ):
		if gamestate.framerate == 0:
			gamestate.framerate = 1
		elif gamestate.framerate == 1:
			gamestate.framerate = 250
		else:
			gamestate.framerate *= 2
	elif config.keys_skip_to_end.matches( event ):
		gamestate.framerate = 1000000
	else:
		gamestate.key_events.append( event )


# ----------------------

def finishedgame_input( event, waiting ):
	if event.type == QUIT:
		sys.exit(0)
	elif config.keys_volup.matches( event ):
		config.volume = sound_mgr.increase_volume()
		config.save()
	elif config.keys_voldown.matches( event ):
		config.volume = sound_mgr.decrease_volume()
		config.save()
	elif config.keys_startgame.matches( event ):
		waiting = False
	return waiting

# ----------------------

class PlayerStatus:
	def __init__( self, x, y, direction, player ):
		self._x = x
		self._y = y
		self._dir = direction
		self._colour = player.GetColour()
		self._dead = False

	def GetPosition( self ):
		return ( self._x, self._y )

	def GetDirection( self ):
		return ( self._dir )

	def SetDirection( self, direction ):
		self._dir = direction

	def SetDead( self, dead ):
		self._dead = dead

	def IsDead( self ):
		return self._dead

	def Move( self, gamestate ):
		"""Moves the player one position depending on its direction, and
		returns True if it hit anything, False otherwise."""

		if self._dead:
			return self._dead

		if self._dir == DIR_UP:
			self._y -= 1
		elif self._dir == DIR_RIGHT:
			self._x += 1
		elif self._dir == DIR_DOWN:
			self._y += 1
		elif self._dir == DIR_LEFT:
			self._x -= 1

		self._dead = gamestate.AddPixel( self._x, self._y, self._colour )
		if self._dead:
			gamestate.alive_colours.remove( self._colour )

		return self._dead

class GameBoard( BasicGameBoard ):

	def __init__( self, gamestate ):
		self._gamestate = gamestate

	def GetArenaSize( self ):
		return self._gamestate.config.arena_size

	def GetAbsolutePixel( self, pos ):
		if pos in self._gamestate.pixels_set:
			retval = 1
		else:
			retval = 0
		return retval

	def GetPlayerPositions( self, pos_to_exclude = None ):
		ret = []

		for player in self._gamestate.statuses.keys():
			status = self._gamestate.statuses[player]
			if not status.IsDead():
				pos = status.GetPosition()
				if pos_to_exclude != pos:
					ret.append( ( pos, status.GetDirection() ) )
		return ret

def class_is_human( cls ):
	return  "IsHuman" in cls.__dict__ and cls.IsHuman()

class GameState:
	def __init__( self, config, classes ):
		self.config = config

		self.players = []
		self.key_events = []
		self.any_humans = False

		for cls in classes:
			if class_is_human( cls ):
				self.players.append( cls( config.keys_p1_up,
					config.keys_p1_right, config.keys_p1_down,
					config.keys_p1_left ) )
				self.any_humans = True
			else:
				self.players.append( cls() )

		self.alive = INGAME_TWO_ALIVE
		self.dim = 0.5

		self.pixels_list = []
		self.pixels_set = set()
		self.create_initial_pixels()

		self.alive_colours = []
		self.statuses = {}
		for player in self.players:
			x = random.randint( config.starting_border, config.arena_size[0]
				- config.starting_border )
			y = random.randint( config.starting_border, config.arena_size[1]
				- config.starting_border )
			dr = random.randint( DIR_UP, DIR_LEFT )
			self.statuses[player] = PlayerStatus( x, y, dr, player )
			self.AddPixel( x, y, player.GetColour() )
			self.alive_colours.append( player.GetColour() )

		self._gameboard = GameBoard( self )

	def timer_tick( self ):
		num_alive = 0
		for player in self.players:
			status = self.statuses[player]
			if not status.IsDead():
				if class_is_human( player.__class__ ):
					new_dir = player.GetDirWithInput( status.GetDirection(),
						self.key_events )
				else:
					try:
						new_dir = player.GetDir( status.GetPosition(),
							status.GetDirection(), self._gameboard )
					except Exception, e:
						sys.stderr.write( ( "Player '%s' threw "
							+ "an exception from GetDir().  "
							+ "Killing it.\nException: '%s'\n" ) %
							( player.GetName(), e ) )
						new_dir = DIR_UP
						status.SetDead( True )

					if new_dir not in (DIR_UP, DIR_RIGHT, DIR_DOWN, DIR_LEFT):
						sys.stderr.write( "Player '" + player.GetName()
							+ "' returned an invalid value from GetDir().  Killing it.\n" )
						new_dir = DIR_UP
						status.SetDead( True )

				status.SetDirection( new_dir )
				# TODO: copy pixels list for "security"?

				dead = self.statuses[player].Move( self )
				if not dead:
					num_alive += 1

		if num_alive < 2:
			self.alive = INGAME_MOST_DEAD

	def create_initial_pixels( self ):
		colour = (100, 100, 100)
		size_x, size_y = config.arena_size
		for x in range( size_x ):
			self.AddPixel( x, 0, colour )
			self.AddPixel( x, size_y - 1, colour )
		for y in range( 1, size_y - 1 ):
			self.AddPixel( 0, y, colour )
			self.AddPixel( size_x - 1, y, colour )

	def GetPosition( self, player ):
		return self.statuses[player].GetPosition()

	def AddPixel( self, x, y, colour ):
		if ( x, y ) in self.pixels_set:
			dead = True
		else:
			dead = False
			self.pixels_set.add( ( x, y ) )

		self.pixels_list.append( ( x, y, colour ) )

		return dead


# ----------------------

def ingame_mainloop( config ):

	scores = {}
	for cls in config.player_classes:
		scores[cls] = 0

	while True:
		gamestate = GameState( config, config.player_classes )
		inlevel_mainloop( config, gamestate, scores )
		if gamestate.alive == INGAME_QUIT:
			break

	return gamestate

def increment_scores( gamestate, scores ):
	for player in gamestate.players:
		if not gamestate.statuses[player]._dead:
			scores[player.__class__] += 1

# ----------------------

def inlevel_mainloop( config, gamestate, scores ):

	gamestate.alive = INGAME_TWO_ALIVE

	inlevel_redraw_screen( gamestate, True, scores )
	if gamestate.any_humans:
		time.sleep( 1 )
	inlevel_redraw_screen( gamestate, False, scores )

	gc.disable()
	tick_counter = 0

	gamestate.framerate = 0
	num_alive = len(gamestate.alive_colours)

	while gamestate.alive == INGAME_TWO_ALIVE:
		for evt in pygame.event.get():
			inlevel_input( evt, gamestate, scores )
		gamestate.timer_tick()
		gamestate.key_events = []
		tick_counter += 1
		if gamestate.alive != INGAME_MOST_DEAD:
			if gamestate.framerate <= 1:
				inlevel_update_screen( gamestate, scores )
				if gamestate.framerate == 0:
					pygame.time.wait( config.tick_time )
			elif tick_counter >= gamestate.framerate:
				tick_counter = 0
				inlevel_redraw_screen( gamestate, False, scores )
		if num_alive != len(gamestate.alive_colours):
			num_alive = len(gamestate.alive_colours)
			# redraw with dead players dimmed
			inlevel_redraw_screen( gamestate, False, scores )


	gc.enable()
	mopelib.clear_events( EVENTTYPE_INGAME_TICK )
	increment_scores( gamestate, scores )

	finishedlevel_mainloop( gamestate, scores )

	ingame_surface_background.fill( config.colour_background )


# ----------------------

def write_text( txt, colour, size, y_pos ):
	ft = pygame.font.Font( None, int( config.screen_size[1] * size ) )
	sf = ft.render( txt, True, colour )
	screen.blit( sf, ( (config.screen_size[0] - sf.get_width() )/2,
		(config.screen_size[1] - sf.get_height() ) * y_pos ) )

# ----------------------

def write_text_ingame( gamestate, scores ):
	global ingame_font

	txt = ""
	for cls in scores.keys():
		txt += "%s: %d   " % ( cls.GetName(), scores[cls] )

	colour = (128, 128, 128)
	bgcolour = config.colour_background
	y_pos = 0.996
	sf = ingame_font.render( txt, True, colour )
	sf_bg = pygame.Surface( ( int( sf.get_width() ), sf.get_height() ) )
	sf_bg.fill( bgcolour )

	tlx = ( config.screen_size[0] - sf_bg.get_width() ) / 2
	tly = ( config.screen_size[1] - sf_bg.get_height() ) * y_pos

	dirty_rect = Rect( tlx, tly, sf_bg.get_width(), sf_bg.get_height() )
	gamestate.offscreen_buff.blit( sf_bg, dirty_rect )
	gamestate.offscreen_buff.blit( sf, ( tlx * 1.01, tly ) )

# ----------------------

def finishedlevel_input( event, waiting, gamestate, scores ):
	if event.type == QUIT:
		sys.exit(0)
	elif( ( event.type == pygame.ACTIVEEVENT and event.state == 2 )
	   or config.keys_menu.matches( event ) ):
		general_menu_screen( config, gamestate )
		inlevel_redraw_screen( gamestate, False, scores )
		if gamestate.alive == INGAME_QUIT:
			waiting = False
	elif config.keys_volup.matches( event ):
		config.volume = sound_mgr.increase_volume()
		config.save()
	elif config.keys_voldown.matches( event ):
		config.volume = sound_mgr.decrease_volume()
		config.save()
	elif event.type == EVENTTYPE_TITLE_TICK:
		waiting = False
	elif config.keys_startgame.matches( event ):
		waiting = False
	return waiting

def finishedlevel_mainloop( gamestate, scores ):

	if not gamestate.any_humans:
		pygame.time.set_timer( EVENTTYPE_TITLE_TICK, TITLE_TICK_TIME )

	inlevel_redraw_screen( gamestate, False, scores )
	waiting = True
	while waiting:
		waiting = finishedlevel_input( pygame.event.wait(), waiting, gamestate, scores )

	if not gamestate.any_humans:
		mopelib.clear_events( EVENTTYPE_TITLE_TICK )

def finishedgame_mainloop( config, gamestate ):
	pass
	#config.start_level = 0
	#	config.save()
	#
	#	inlevel_redraw_screen( gamestate )
	#	write_text( "Congratulations!", (255,255,255), 0.125, 0.38 )
	#	write_text( "You won!", (255,255,255), 0.125, 0.52 )
	#	waiting = True
	#	write_text( "Press %s" % config.keys_startgame.name, (255,255,255),
	#		0.05, 0.8 )
	#	pygame.display.update()
	#	while waiting:
	#		waiting = finishedgame_input( pygame.event.wait(), waiting )
	#
	#	ingame_surface_background.fill( config.colour_background )

def sort_by_score( class2score ):
	scorename_sorted = []

	for cls in class2score.keys():
		scorename_sorted.append( ( class2score[cls], cls.GetName() ) )

	scorename_sorted.sort( reverse = True )

	return scorename_sorted

def execute_tests( config ):
	module_name = config.test_player_name
	module = __import__( "players." + module_name,
		globals(), locals() ).__dict__[module_name]

	module.run_tests()

def execute_tournament( config ):
	total_scores = {}

	classes = []
	for cls in config.player_classes:
		if not class_is_human( cls ):
			classes.append( cls )
			total_scores[cls] = 0

	print
	print " " * 30 + "=== Pairings ==="
	print

	pairs = []
	num_classes = len( classes )
	for i in range( num_classes ):
		for j in range( i+1, num_classes ):
			execute_match(
				( classes[i], classes[j] ),
					total_scores, False, config.num_games )

	print
	print " " * 30 + u"=== Melee ==="
	print

	execute_match( classes, total_scores, True,
		config.num_games * (len( classes )-1) )

	print
	print " " * 30 + "=== Total Scores ==="
	print

	scorename_sorted = sort_by_score( total_scores )

	for score, name in scorename_sorted:
		print "%30s % 4d" % ( name, score )
	print


def execute_match( classes, total_scores, newlines, num_games ):
	match_scores = {}
	for cls in classes:
		match_scores[cls] = 0

	print_dots = False

	for i in range( num_games ):
		if print_dots:
			sys.stdout.write( "." )
		gamestate = GameState( config, classes )

		while gamestate.alive == INGAME_TWO_ALIVE:
			gamestate.timer_tick()

		for player in gamestate.statuses.keys():
			status = gamestate.statuses[player]
			if not status.IsDead():
				match_scores[player.__class__] += 1

	if print_dots:
		print

	scorename_sorted = sort_by_score( match_scores )

	oldscore = -1
	for score, name in scorename_sorted:
		if oldscore != -1 and not newlines:
			if oldscore == score:
				print " drew",
			else:
				print " beat",
			print "%- 4d %-30s" % ( score, name ),
		else:
			print "%30s% 4d" % ( name, score ),
		oldscore = score


		if newlines:
			print

	if not newlines:
		print

	for cls in classes:
		total_scores[cls] += match_scores[cls]

def get_players( players_dir ):
	ret_classes = []

	player_re = re.compile( """^(\w*Player).py$""" )
	for filename in os.listdir( players_dir ):
		m = player_re.match( filename )
		if m:
			class_name = m.group( 1 )
			cls = __import__( "players." + class_name,
				globals(), locals() ).__dict__[class_name].__dict__[class_name]
			ret_classes.append( cls )

	#return ret_classes[:2]
	return ret_classes

# ----------------------
# Execution starts here
# ----------------------

# Fixed constants

INTRO_MODE_TITLE = 0
INTRO_MODE_INSTR = 1
INTRO_MODE_MUSIC = 2
INTRO_MODE_ENDED = 3

MENU_START         = 0
MENU_NUM_PLAYERS   = 1
MENU_END           = 2
MENU_MUSIC         = 3
MENU_SOUND_EFFECTS = 4
MENU_QUIT          = 6
MENU_CHANGE_PLAYER = 100 # Must go at end of this list

INGAME_TWO_ALIVE = 0
INGAME_MOST_DEAD = 1
INGAME_QUIT      = 2

TITLE_TICK_TIME  = 4000

EVENTTYPE_INGAME_TICK = pygame.USEREVENT
EVENTTYPE_TITLE_TICK  = pygame.USEREVENT + 1

num_args = len( sys.argv )

config_filename = os.path.expanduser( "~/.troncode/config" )
install_dir = "."

run_tests = False
run_tournament = False

config = TronCodeConfig( config_filename )

if num_args > 1:
	if sys.argv[1] == "--tournament":
		run_tournament = True
		config.num_games = 100
	elif sys.argv[1] == "--test":
		if num_args > 2:
			run_tests = True
			config.test_player_name = sys.argv[2]
			config.unsaved.append( "test_player_name" )
		else:
			sys.stderr.write( "Please supply the name of the player to test.\n" )
			sys.exit( 1 )

config.install_dir = install_dir
config.unsaved.append( "install_dir" )

config.images_dir = os.path.join( install_dir, "images" )
config.unsaved.append( "images_dir" )

config.music_dir = os.path.join( install_dir, "music" )
config.unsaved.append( "music_dir" )

if num_args > 3:
	config.screen_size = config.parse_value( sys.argv[3] )

config.arena_size = ( 200, 200 )
config.unsaved.append( "arena_size" )

config.starting_border = 35
config.unsaved.append( "starting_border" )

config.players_dir = "players"
config.unsaved.append( "players_dir" )

config.player_classes = get_players( config.players_dir )
config.unsaved.append( "player_classes" )

config.players = []
config.unsaved.append( "players" )

if run_tests:
	execute_tests( config )
elif run_tournament:
	execute_tournament( config )
else:
	pygame.init()
	pygame.font.init()

	window = pygame.display.set_mode( config.screen_size )
	pygame.display.set_caption( 'troncode' )
	screen = pygame.display.get_surface()

	fixed_border = 5
	bottom_border = 25
	scale = min(
		( float( config.screen_size[0] - fixed_border*2 )
			/ float( config.arena_size[0] ) ),
		( float( config.screen_size[1] - ( fixed_border*2 + bottom_border ) )
			/ float( config.arena_size[1] ) ) )

	screen_border = ( float( config.screen_size[0] - config.arena_size[0]*scale )
		/ 2.0,
		float( ( config.screen_size[1] - config.arena_size[1]*scale ) - ( bottom_border - fixed_border ) ) / 2.0 )

	ceil_scale = math.ceil( scale )

	# General initialisation

	num_joysticks = pygame.joystick.get_count()
	for j in range( num_joysticks ):
		pygame.joystick.Joystick( j ).init()

	intro_surface_title = mopelib.load_and_scale_image( "title.png", config )
	intro_surface_instr = mopelib.load_and_scale_image( "instructions.png", config )
	intro_surface_music = mopelib.load_and_scale_image( "music.png", config )

	ingame_surface_background = pygame.Surface( screen.get_size() ).convert()
	ingame_surface_background.fill( config.colour_background )

	intro_mode = INTRO_MODE_TITLE

	sound_mgr = TronCodeSoundManager( config.volume )

	troncode_version = mopelib.read_version( config )

	ingame_font = pygame.font.Font( None, int( config.screen_size[1] * 0.019 ) )

	menurender = mopelib.MenuRenderer( screen, config, ingame_surface_background,
		(128, 128, 128), (128, 255, 128), (128, 128, 128) )

	while True:
		sound_mgr.music_loud()
		intro_mainloop( config )
		sound_mgr.music_quiet()
		gamestate = ingame_mainloop( config )
		intro_mode = finishedgame_mainloop( config, gamestate )


