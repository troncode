from troncode_values import *

# traps
TRAP_1 = "L L LRR R   R  L "
TRAP_2 = "R R RLL L   L  R "


class ArbenPlayer( object ):
	"""Testing my cobra"""

	def __init__( self ):
		pass

	def GetColour( self ):
		return ( 99, 99, 99 )

	def GetName():
		return "Arben's player"
	GetName = staticmethod( GetName )

	def GetDir( self, position, direction, gameboard ):
		"""Do something cunning that cannot fail"""

		ret_dir = direction
		
		# Build trap # change to new design
		if ( gameboard.GetRelativePixel( position, direction, 2, 1 ) > 0 or
			gameboard.GetRelativePixel( position, direction, 1, -2 ) > 0 ) :			
			self.sequence = TRAP_2;			
			ret_dir = gameboard.TurnRight( ret_dir )
			
		elif ( gameboard.GetRelativePixel( position, direction, 2, 1 ) > 0 or
			gameboard.GetRelativePixel( position, direction, 1, 2 ) > 0 ) :			
			self.sequence = TRAP_1;			
			ret_dir = gameboard.TurnLeft( ret_dir )			
		
		#
		#	if (action == "R") :
		#		return gameboard.TurnRight( direction )
		#	elif (action == "L") :
		#		return gameboard.TurnLeft( direction )
		#
		
		# Turn right if we can
		if gameboard.GetRelativePixel( position, direction, 1, 2 ) == 0:
			ret_dir = gameboard.TurnRight( ret_dir )

		# Don't enter tunnels of width 2 or less unless there is no other space
		if ( gameboard.GetRelativePixel( position, ret_dir, 2, 1 ) > 0 and
			 gameboard.GetRelativePixel( position, ret_dir, 2, -1 ) > 0 ):
			ret_dir = gameboard.TurnLeft( ret_dir )
				
		# Avoid immediate death by turning left
		for i in range( 4 ):
			if gameboard.GetRelativePixel( position, ret_dir, 1, 0 ) == 0:
				break
			ret_dir = gameboard.TurnLeft( ret_dir )
			
	
		return ret_dir