from troncode_values import *

class TurnRightPlayer( object ):
	"""An example player to get you started on writing your own.  This player
	goes straight until it hits a wall, and then it turns right."""

	def __init__( self ):
		"""Do any required setup here."""
		pass

	def GetColour( self ):
		"""Return the colour this light cycle should be displayed on screen."""
		return ( 160, 160, 255 )

	def GetName():
		"""Return the name of this player as it should be displayed."""
		return "Turn Right"
	GetName = staticmethod( GetName )

	def GetDir( self, position, direction, gameboard ):
		"""Turn right if there is a pixel directly in front.
		position is my location - a point (x,y) in the arena
		direction is my direction: DIR_UP, DIR_DOWN, DIR_LEFT or DIR_RIGHT
		gameboard is an AbstractGameBoard which has useful methods on it."""

		if gameboard.GetRelativePixel( position, direction, 1, 0 ) > 0:
			return gameboard.TurnRight( direction )
		else:
			return direction

