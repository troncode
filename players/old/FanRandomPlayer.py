from troncode_values import *
import random
import math

DANGER = 0
SAFE = 1

class FanRandomPlayer( object ):

	def __init__( self ):
		self.step = 0
		self.last = 0

	def GetColour( self ):
		return ( 255, 255, 0 )

	def GetName():
		return "Fan Zhang's Random Player!"
	GetName = staticmethod( GetName )
		
	def ProbeEnemies(self, position, gameboard):
		enemies = gameboard.GetPlayerPositions(position)
		(my_x, my_y) = position
		for (epos, edir) in enemies:
			(x, y) = epos
			dis = math.sqrt((my_x-x)*(my_x-x) + (my_y-y)*(my_y-y))
			if (dis < 3):
				return (DANGER, epos, edir)
		return (SAFE, None, None)

	def GetDir( self, position, direction, gameboard ):

		ret_dir = direction
		
		#starting direction
		(x, y) =  position
		ax = math.fabs(100-x)
		ay = math.fabs(100-y)
		if self.step==0 and x < y and x < ay and x < ax:
			ret_dir = DIR_LEFT
		elif self.step==0 and y < x and y < ax and y < ay:
			ret_dir = DIR_DOWN
		elif self.step==0 and ax < ay and ax < y and ax < x:
			ret_dir = DIR_RIGHT
		elif self.step==0 and ay < ax and ay < x and ay < y:
			ret_dir = DIR_UP
		
		#when meet the wall
		if (x == 0 or x == 199) and y > ay:
			ret_dir = DIR_DOWN
		elif (x == 0 or x == 199) and y <= ay:
			ret_dir = DIR_UP
		elif (y == 0 or y == 199) and x > ax:
			ret_dir = DIR_LEFT
		elif (y == 0 or y == 199) and x <= ax:
			ret_dir = DIR_RIGHT				
		
		#normal direction for marching
		if self.last == 1:
			ret_dir = gameboard.TurnLeft( ret_dir )
			self.last = 0
		else:
			ret_dir = gameboard.TurnLeft( ret_dir )
			self.last = 1
			
		player = self.ProbeEnemies(position, gameboard)
		
		if player[0] == DANGER:
			ret_dir = gameboard.TurnLeft( ret_dir )

		# Don't enter tunnels of width 1
		if ( gameboard.GetRelativePixel( position, ret_dir, 1, 1 ) > 0 and
			 gameboard.GetRelativePixel( position, ret_dir, 1, -1 ) > 0 ):
			ret_dir = gameboard.TurnRight( ret_dir )

		# Avoid immediate death by turning right
		for i in range( 4 ):
			if gameboard.GetRelativePixel( position, ret_dir, 1, 0 ) == 0:
				break
			ret_dir = gameboard.TurnRight( ret_dir )
			
		self.step = self.step+1
		return ret_dir
