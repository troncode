from troncode_values import *

class NullPlayer( object ):

	def __init__( self ):
		pass

	def GetColour( self ):
		return ( 255, 255, 255 )

	def GetName():
		return "Null Player"
	GetName = staticmethod( GetName )

	def GetDir( self, position, direction, gameboard ):
		return direction
