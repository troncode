from troncode_values import *
import math

class FollowerPlayer( object ):
    """Follower Player."""

    # Matrix of
    # LeftOrRight{-1,0,1} X UpOrDown{-1,0,1} -> Dir{ 0, 1, 2, 3 }
    whereTable  = [ [ 0, 3, 0 ],
                    [ 0, None, 2 ],
                    [ 2, 1, 2 ] ]
    
    # Matrix of
    # CurrentDir{0,1,2,3} X NextDir{0,1,2,3} -> Dir( -1: Left, None: Left/Right, 1: Right )
    dirTable = [ [ 0, 1, None, -1 ],
                 [ -1, 0, 1, None ],
                 [ None, -1, 0, 1 ],
                 [ 1, None, -1, 0 ] ]
        
    def __init__( self ):
        self.m_step = 0

    def GetColour( self ):
        return ( 255, 128, 0 )

    def GetName():
        return "Follower"
    GetName = staticmethod( GetName )

    def GetDir( self, position, direction, gameboard ):
        """Stick as close as possible to any wall you find."""

        pos = gameboard.GetPlayerPositions( )
        #print "my-position: ", position
        #print "my-direction: ", direction
        for p in pos:
            if p[0] == position:
                pass
            else:
                #print "your-position: ", p[0]
                #print "your-direction: ", p[1]
                if p[0][0] < position[0]:
                    leftOrRight = -1
                elif  p[0][0] == position[0]:
                    leftOrRight = 0
                else:
                    leftOrRight = 1
                    
                if p[0][1] < position[1]:
                    upOrDown = -1
                elif  p[0][1] == position[1]:
                    upOrDown = 0
                else:
                    upOrDown = 1
                    
                #print "LeftOrRight: ", leftOrRight 
                #print "UpOrDown: ", upOrDown
                
                where = FollowerPlayer.whereTable[ leftOrRight + 1 ][upOrDown + 1 ]
                #print "---> where: ", where
                if where is None:
                    pass
                elif where == -1 :
                    pass
                else:
                    dir = FollowerPlayer.dirTable[ direction ][ where ]
                    #print "---> dir: ", dir
                    if dir==-1 or dir is None:
                        pass
                    else:
                        return dir
                
        self.m_step += 1
        
        ret_dir = direction

        # Turn left if we can
        if gameboard.GetRelativePixel( position, direction, 0, -1 ) == 0:
            ret_dir = gameboard.TurnLeft( ret_dir )

        # Don't enter tunnels of width 1
        if ( gameboard.GetRelativePixel( position, ret_dir, 1, 1 ) > 0 and
             gameboard.GetRelativePixel( position, ret_dir, 1, -1 ) > 0 ):
            ret_dir = gameboard.TurnRight( ret_dir )

        # Avoid immediate death by turning right
        for i in range( 4 ):
            if gameboard.GetRelativePixel( position, ret_dir, 1, 0 ) == 0:
                break
            ret_dir = gameboard.TurnRight( ret_dir )

        return ret_dir
