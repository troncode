from troncode_values import *

TRAP_GAP = 5

class BobsPlayer( object ):

	def __init__( self ):
		self.counter = 0
		self.trap_gap = TRAP_GAP

	def GetColour( self ):
		return ( 255, 255, 255 )

	def GetName():
		return "Bobs Player"
	GetName = staticmethod( GetName )

	def GetAnyWallsInRangeAhead( self, position, direction, gameboard ):
		rval = 0
		for i in range( 1, self.trap_gap ):
			if gameboard.GetRelativePixel(  position, direction, i, 0 ) > 0:
				rval = rval+1

		return rval

	def GetAnyWallsInRangeRight( self, position, direction, gameboard ):
		rval = 0

		for i in range( -1, 1 ):
			for j in range( 1, self.trap_gap ):
				if gameboard.GetRelativePixel(  position, direction, i, j ) > 0:
					rval = rval+1

		return rval

	def GetAnyWallsInRangeLeft( self, position, direction, gameboard ):
		rval = 0

		for i in range( -1, 1 ):
			for j in range( -1, -self.trap_gap ):
				if gameboard.GetRelativePixel(  position, direction, i, j ) > 0:
					rval = rval+1

		return rval

	def GetDir( self, position, direction, gameboard ):
		"""Turn Left Player but turn right occasionally"""

		ret_dir = direction

		density_ahead = self.GetAnyWallsInRangeAhead( position, direction, gameboard )
		density_left = self.GetAnyWallsInRangeLeft( position, ret_dir, gameboard )
		density_right = self.GetAnyWallsInRangeRight( position, ret_dir, gameboard )

		#Avoid hitting something straight ahead up to self.trap_gap pixels away
		if ( density_ahead > 0 ):
			if ( density_left == 0 ) :
				ret_dir = gameboard.TurnLeft( direction )
				self.counter=self.counter+1

			elif ( density_right == 0 ) :
				ret_dir = gameboard.TurnRight( direction )
				self.counter=self.counter-1

			elif ( density_left <  density_right ) :
				ret_dir = gameboard.TurnLeft( direction )
				self.counter=self.counter+1

			elif ( density_right < density_left  ) :
				ret_dir = gameboard.TurnRight( direction )
				self.counter=self.counter-1

		elif ( self.counter > 2 and density_right == 0 ) :
				ret_dir = gameboard.TurnRight( direction )

		# if we're stuck in a box of our own making, best to try and stay alive as long as possible..
		elif ( self.counter > 4 or self.counter < -4 ) and ( self.trap_gap > 2 ) :
			self.trap_gap=self.trap_gap-1

		return ret_dir



