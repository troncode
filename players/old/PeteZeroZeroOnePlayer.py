from troncode_values import *
import time
TRAP_SEQUENCE_R = "R RLL   LLR R"
TRAP_SEQUENCE_L = "L LRR   RRL L  "

TRAP_GAP = 20
TRAP_MULT = 1.6
class PeteZeroZeroOnePlayer ( object ):

    def __init__( self ):
        self.players = ()
        self.previousDirection = -1
        self.gameBoardX = -1
        self.gameBoardY = -1
        self.counter = TRAP_GAP
        self.real_dir = DIR_UP
        self.trap_gap = TRAP_GAP

        pass

    def GetColour( self ):
        return ( 255, 255, 254 )

    def GetName():
        return "Pete Zero Zero One"
    GetName = staticmethod( GetName )

    def GetDir( self, position, direction, gameboard ):

        if self.previousDirection == -1:
            self.previousDirection = direction

        returnDirection = direction


        #calculate the size of the board
        if self.gameBoardY == -1:
            self.gameBoardX = position[0]
            self.gameBoardY = position[1]
            tempPosition = self.gameBoardX, self.gameBoardY

            x = True
            while x == True:
                if gameboard.GetRelativePixel( tempPosition, DIR_RIGHT, 0, 3) == 0:
                    self.gameBoardY = self.gameBoardY + 3;
                    tempPosition = self.gameBoardX,self.gameBoardY
                else:
                    x = False

            x = True
            while x == True:
                if gameboard.GetRelativePixel( tempPosition, DIR_RIGHT, 3, 0) == 0:
                    self.gameBoardX = self.gameBoardX + 3;
                    tempPosition = self.gameBoardX,self.gameBoardY
                else:
                    x = False


        if position[0] > ( self.gameBoardX / 2 ):
            returnDirection = DIR_LEFT
        elif position[0] < ( self.gameBoardX / 2 ):
            returnDirection = DIR_RIGHT
        else:
            if position[1] > ( self.gameBoardY / 2 ):
                returnDirection = DIR_DOWN
            elif position[1] < ( self.gameBoardY / 2 ):
                returnDirection = DIR_UP


    #Shamelessly pillaged from the code of others
        # Avoid our own traps by not entering tunnels of width 1
        if ( gameboard.GetRelativePixel( position, returnDirection, 1, 1 ) > 0 and
             gameboard.GetRelativePixel( position, returnDirection, 1, -1 ) > 0 ):
            returnDirection = gameboard.TurnRight( returnDirection )
            self.counter = self.trap_gap
        # Avoid immediate death by turning back the way we were going
        if gameboard.GetRelativePixel( position, returnDirection, 1, 0 ) > 0:
            returnDirection = self.real_dir
            self.counter = self.trap_gap
        # Avoid immediate death by turning right
        for i in range( 4 ):
            if gameboard.GetRelativePixel( position, returnDirection, 1, 0 ) == 0:
                break
            returnDirection = gameboard.TurnRight( returnDirection )
            self.counter = self.trap_gap




        self.previousDirection = returnDirection
        """time.sleep(0.1)"""
        return returnDirection
