from troncode_values import *

TRAP_SEQUENCE_R = "R RLL   LLR R"
TRAP_SEQUENCE_L = "L LRR   RRL L  "

TRAP_GAP = 20
TRAP_MULT = 1.6

class TurnRightKillerPlayer( object ):

	def __init__( self ):
		self.counter = TRAP_GAP
		self.real_dir = DIR_UP
		self.trap_gap = TRAP_GAP

	def GetColour( self ):
		return ( 50, 50, 255 )

	def GetName():
		return "Turn-Right-Killer"
	GetName = staticmethod( GetName )

	def GetDir( self, position, direction, gameboard ):
		"""Set traps for the TurnRightPlayer (and similar) and avoid them ourselves"""

		# Build trap
		ret_dir = direction
		if self.counter == 1:
			self.real_dir = direction
			self.sequence = ""
			# If there's nothing on our left, pattern left
			if gameboard.GetRelativePixel( position, ret_dir, 0, -1 ) == 0:
				self.sequence += TRAP_SEQUENCE_L
			# If there's nothing on our right, pattern right
			if gameboard.GetRelativePixel( position, ret_dir, 0, 1 ) == 0:
				self.sequence += TRAP_SEQUENCE_R

		elif self.counter <= 0:
			rev_counter = -self.counter

			if rev_counter >= len( self.sequence ):
				self.trap_gap = int( self.trap_gap * TRAP_MULT )
				self.counter = self.trap_gap
			else:
				trap_char = self.sequence[rev_counter]
				if trap_char == "L":
					ret_dir = gameboard.TurnLeft( direction )
				elif trap_char == "R":
					ret_dir = gameboard.TurnRight( direction )

		# Avoid our own traps by not entering tunnels of width 1
		if ( gameboard.GetRelativePixel( position, ret_dir, 1, 1 ) > 0 and
			 gameboard.GetRelativePixel( position, ret_dir, 1, -1 ) > 0 ):
			ret_dir = gameboard.TurnRight( ret_dir )
			self.counter = self.trap_gap

		# Avoid immediate death by turning back the way we were going
		if gameboard.GetRelativePixel( position, ret_dir, 1, 0 ) > 0:
			ret_dir = self.real_dir
			self.counter = self.trap_gap

		# Avoid immediate death by turning right
		for i in range( 4 ):
			if gameboard.GetRelativePixel( position, ret_dir, 1, 0 ) == 0:
				break
			ret_dir = gameboard.TurnRight( ret_dir )
			self.counter = self.trap_gap

		self.counter -= 1
		return ret_dir
