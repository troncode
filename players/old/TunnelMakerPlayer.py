from troncode_values import *

MODE_START = 0
MODE_NORMAL = 3
MODE_SEQUENCE = 5

COLLAPSE_SEQUENCE = "LL-RZ3"
SEQUENCE_STEP_AWAY = "LR-Z3"
SEQUENCE_STEP_IN = "RL-Z3"

class TunnelMakerPlayer( object ):
	"""An example player to get you started on writing your own.  This player
	creates tunnels to trap opponents."""

	def __init__( self ):
		"""Do any required setup here."""
		self.state = MODE_START

	def GetColour( self ):
		"""Return the colour this light cycle should be displayed on screen."""
		return ( 218, 165, 32 )

	def GetName():
		"""Return the name of this player as it should be displayed."""
		return "Edmund's Tunnel Maker"
	GetName = staticmethod( GetName )


	def DebugPrint( self, what ) :
#		if ( True ) :
		if ( False ) :
			print what

	def IssueMove( self, gameboard, position, new_direction ) :
		"""Move in the direction indicated, if not blocked"""

		for i in range (4) :
			if gameboard.GetRelativePixel( position, new_direction, 1, 0 ) == 0:
				return  new_direction
			else :
				new_direction = gameboard.TurnLeft( new_direction )

		self.DebugPrint("Trying " + str(new_direction) + "...")
		return  new_direction

	def CountOccupied(self, gameboard, position, direction, forward_range, right_range) :
		"""How many of this block are occupied?"""

		count= 0
		for df in forward_range :
			for ds in right_range :
				if gameboard.GetRelativePixel( position, direction, df, ds ) > 0:
					count+= 1

		return  count


	def GetDir( self, position, direction, gameboard ):
		"""Repeatedly create a tunnel to lure an opposing player into, then close it
		so they can't get out"""

		if (self.state == MODE_START or self.state == MODE_NORMAL) :
			# Skim this obstacle
			if (gameboard.GetRelativePixel( position, direction, 1, 0 ) > 0 and
			    gameboard.GetRelativePixel( position, direction, 1, -1 ) == 0) :
				self.state = MODE_SEQUENCE
				self.sequence = SEQUENCE_STEP_AWAY
				self.DebugPrint("skim")
				return  self.GetDir ( position, direction, gameboard )

			# Found "the side", so collapse the tunnel
			if (gameboard.GetRelativePixel( position, direction, 1, 0 ) > 0 or
			    gameboard.GetRelativePixel( position, direction, 1, -1 ) > 0) :
				self.state = MODE_SEQUENCE
				self.sequence = COLLAPSE_SEQUENCE;
				self.DebugPrint("jaws")
				self.PrintRadar(gameboard, position, direction)
				return  self.GetDir ( position, direction, gameboard )

			# Too close -> step out
			if (gameboard.GetRelativePixel( position, direction, 0, 1 ) > 0 or
			    gameboard.GetRelativePixel( position, direction, 0, 2 ) > 0) :
				self.state = MODE_SEQUENCE
				self.sequence = SEQUENCE_STEP_AWAY
				self.DebugPrint("step out")
				return  self.GetDir ( position, direction, gameboard )

			# Turn right if there's space
			elif (self.state == MODE_NORMAL and
			      self.CountOccupied( gameboard, position, direction,
						  range(-2, 1), range(1, 4)) == 0) :
				self.DebugPrint("right")
				return gameboard.TurnRight( direction )

			else :
				return self.IssueMove(gameboard, position, direction)


		if (self.state == MODE_SEQUENCE) :
			action = self.sequence[0]
			self.sequence = self.sequence[1:]
			self.DebugPrint("S[" + action + "]")
			if (action == "Z") :
				self.state = int(self.sequence[0])
				self.DebugPrint("Z = " + str(self.state))
				return  self.GetDir ( position, direction, gameboard )
			elif (action == "R") :
				return gameboard.TurnRight( direction )
			elif (action == "L") :
				return gameboard.TurnLeft( direction )
			elif (action == "-") :
				return self.IssueMove(gameboard, position, direction);
			else :
				print "Gack!"

		print "no direction!!!\a"


	def PrintRadar( self, gameboard, position, direction ) :
		"""Debug dump of what we can see"""

#		print "" + str(gameboard.GetRelativePixel(position, direction, 2, -1)) + "-" + \
#		    str(gameboard.GetRelativePixel(position, direction, 2, 0)) + "-" + \
#		    str(gameboard.GetRelativePixel(position, direction, 2, 1))

#		print str(gameboard.GetRelativePixel(position, direction, 1, -1)) + "-" + \
#		    str(gameboard.GetRelativePixel(position, direction, 1, 0)) + "-" + \
#		    str(gameboard.GetRelativePixel(position, direction, 1, 1))
#		print str(gameboard.GetRelativePixel(position, direction, 0, -1)) + "-" + \
#		    "^" + "-" + \
#		    str(gameboard.GetRelativePixel(position, direction, 0, 1))

