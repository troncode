from troncode_values import *

class HumanPlayer( object ):

	def __init__( self, event_up, event_right, event_down, event_left ):
		self._event_up    = event_up
		self._event_right = event_right
		self._event_down  = event_down
		self._event_left  = event_left

	def GetColour( self ):
		return ( 128, 255, 128 )

	def IsHuman():
		return True
	IsHuman = staticmethod( IsHuman )

	def GetName():
		return "Human"
	GetName = staticmethod( GetName )

	def GetDirWithInput( self, direction, key_events ):
		for evt in key_events:
			if direction != DIR_DOWN and self._event_up.matches( evt ):
				return DIR_UP
			elif direction != DIR_LEFT and self._event_right.matches( evt ):
				return DIR_RIGHT
			elif direction != DIR_UP and self._event_down.matches( evt ):
				return DIR_DOWN
			elif direction != DIR_RIGHT and self._event_left.matches( evt ):
				return DIR_LEFT
		return direction


