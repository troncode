from troncode_values import *

STATE_START  = 0  # Start
STATE_CUT1   = 1  # First part of cutoff step
STATE_TURN   = 2  # Turning around
STATE_CUT2   = 3  # Second part of cutoff step
STATE_NORMAL = 4  # Normal wallfollower behaviour

ORIENTATION_UNKNOWN    = -1
ORIENTATION_VERTICAL   = 0
ORIENTATION_HORIZONTAL = 1

class SelfishPlayer( object ):
	"""Makes a nice space for itself, then follows walls.
	by Andy Balaam"""

	def __init__( self ):
		self.state = STATE_START
		self.orientation = ORIENTATION_UNKNOWN

	def GetColour( self ):
		return ( 255, 255, 255 )

	def GetName():
		return "Selfish"
	GetName = staticmethod( GetName )

	def GetShortLongDir( self, coord, pasthalf_dir, lessthanhalf_dir ):
		if coord > ( 200 - coord ):
			return pasthalf_dir
		else:
			return lessthanhalf_dir

	def GetShortestVerticalDir( self, y_pos ):
		return self.GetShortLongDir( y_pos, DIR_DOWN, DIR_UP )

	def GetLongestVerticalDir( self, y_pos ):
		return self.GetShortLongDir( y_pos, DIR_UP, DIR_DOWN )

	def GetShortestHorizontalDir( self, x_pos ):
		return self.GetShortLongDir( x_pos, DIR_RIGHT, DIR_LEFT )

	def GetLongestHorizontalDir( self, x_pos ):
		return self.GetShortLongDir( x_pos, DIR_LEFT, DIR_RIGHT )

	def FindCut1Dir( self, position ):
		ret_dir = DIR_LEFT

		space_lr = max( position[0], 200 - position[0] )
		space_ud = max( position[1], 200 - position[1] )

		if space_lr > space_ud:
			# Lots of space to left or right: go up or down
			self.orientation = ORIENTATION_VERTICAL
			ret_dir = self.GetShortestVerticalDir( position[1] )
		else:
			# Lots of space above or below: go left or right
			self.orientation = ORIENTATION_HORIZONTAL
			ret_dir = self.GetShortestHorizontalDir( position[0] )

		return ret_dir

	def FindFinalDir( self, position ):
		ret_dir = DIR_LEFT
		if self.orientation == ORIENTATION_VERTICAL:
			ret_dir = self.GetLongestHorizontalDir( position[0] )
		else:
			ret_dir = self.GetLongestVerticalDir( position[1] )
		return ret_dir

	def GetDir( self, position, direction, gameboard ):
		ret_dir = direction
		if self.state == STATE_START:
			ret_dir = self.FindCut1Dir( position )
			self.state = STATE_CUT1

		elif self.state == STATE_CUT1:
			# Turn when we hit a wall
			if gameboard.GetRelativePixel( position, ret_dir, 1, 0 ) != 0:
				ret_dir = gameboard.TurnLeft( ret_dir )
				self.state = STATE_TURN

		elif self.state == STATE_TURN:
			# Keep turning
			ret_dir = gameboard.TurnLeft( ret_dir )
			self.state = STATE_CUT2

		elif self.state == STATE_CUT2:
			if gameboard.GetRelativePixel( position, ret_dir, 1, 0 ) != 0:
				ret_dir = self.FindFinalDir( position )
				self.state = STATE_NORMAL

		else: # STATE_NORMAL
			# Turn left if we can
			if gameboard.GetRelativePixel( position, ret_dir, 0, -1 ) == 0:
				ret_dir = gameboard.TurnLeft( ret_dir )


		# Don't enter tunnels of width 1
		if ( gameboard.GetRelativePixel( position, ret_dir, 1, 1 ) > 0 and
			 gameboard.GetRelativePixel( position, ret_dir, 1, -1 ) > 0 ):
			self.state = STATE_NORMAL
			ret_dir = gameboard.TurnRight( ret_dir )

		# Avoid immediate death by turning right
		for i in range( 4 ):
			if gameboard.GetRelativePixel( position, ret_dir, 1, 0 ) == 0:
				break
			self.state = STATE_NORMAL
			ret_dir = gameboard.TurnRight( ret_dir )

		return ret_dir

