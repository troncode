from troncode_values import *

STATE_BOX         = 0
STATE_SPIRALLING  = 1
STATE_ENTERSPIRAL = 2
STATE_NORMAL      = 3

TURN_LEFT  = 0
TURN_RIGHT = 1

BOX_SIZE = 150

class SpiralPlayer( object ):
	"""Makes room for itself, and then follows walls.
	by Andy Balaam"""

	def __init__( self ):
		self.state = STATE_BOX
		self.box_side_counter = BOX_SIZE
		self.box_corner_counter = 3
		self.box_corner_turndir = TURN_LEFT
		self.start = True

	def GetColour( self ):
		return ( 0, 255, 255 )

	def GetName():
		return "Spiral"
	GetName = staticmethod( GetName )

	def ResetState( self ):
		if self.state != STATE_NORMAL:
			self.state = STATE_ENTERSPIRAL

	def GetTowardCentreDir( self, position ):
		ret_dir = DIR_LEFT
		hor_off = position[0] - 100
		ver_off = position[1] - 100
		if abs( hor_off ) > abs( ver_off ):
			if hor_off > 0:
				ret_dir = DIR_LEFT
			else:
				ret_dir = DIR_RIGHT
		else:
			if ver_off > 0:
				ret_dir = DIR_UP
			else:
				ret_dir = DIR_DOWN

		return ret_dir

	def GetDir( self, position, direction, gameboard ):
		"""Spiral outward until you hit something, then
		stick as close as possible to any wall you find."""

		if self.start:
			self.start = False
			ret_dir = self.GetTowardCentreDir( position )
		else:
			ret_dir = direction

		if self.state == STATE_BOX:
			self.box_side_counter -= 1
			if self.box_side_counter < 0:
				ret_dir = gameboard.TurnLeft( ret_dir )
				self.box_corner_counter -= 1
				if self.box_corner_counter < 0:
					self.state = STATE_SPIRALLING
				else:
					self.box_side_counter = BOX_SIZE
					if self.box_corner_counter == 1:
						self.box_side_counter += 2
					elif self.box_corner_counter == 0:
						self.box_side_counter += 3
		elif self.state == STATE_SPIRALLING:
			# Turn left if we can (3 pixel gap)
			if( gameboard.GetRelativePixel( position, ret_dir, 0, -3 ) == 0 and
				gameboard.GetRelativePixel( position, ret_dir, 0, -2 ) == 0 and
				gameboard.GetRelativePixel( position, ret_dir, 0, -1 ) == 0 and
				gameboard.GetRelativePixel( position, ret_dir, -1, -3 ) == 0 and
				gameboard.GetRelativePixel( position, ret_dir, -1, -2 ) == 0 and
				gameboard.GetRelativePixel( position, ret_dir, -1, -1 ) == 0 and
				gameboard.GetRelativePixel( position, ret_dir, -2, -3 ) == 0 and
				gameboard.GetRelativePixel( position, ret_dir, -2, -2 ) == 0 and
				gameboard.GetRelativePixel( position, ret_dir, -2, -1 ) == 0 ):
					ret_dir = gameboard.TurnLeft( ret_dir )

			# If we're near a wall, turn left so we go inside our spiral
			if( gameboard.GetRelativePixel( position, ret_dir, 2, 0 ) != 0 or
				gameboard.GetRelativePixel( position, ret_dir, 1, 0 ) != 0 ):
					ret_dir = gameboard.TurnLeft( ret_dir )
		elif self.state == STATE_ENTERSPIRAL:
			# Turn left if we can
			if gameboard.GetRelativePixel( position, ret_dir, 0, -1 ) == 0:
				ret_dir = gameboard.TurnLeft( ret_dir )
			self.state = STATE_NORMAL
		else:
			# Turn right if we can
			if gameboard.GetRelativePixel( position, ret_dir, 0, 1 ) == 0:
				ret_dir = gameboard.TurnRight( ret_dir )

		# Don't enter tunnels of width 1
		if ( gameboard.GetRelativePixel( position, ret_dir, 1, 1 ) > 0 and
			 gameboard.GetRelativePixel( position, ret_dir, 1, -1 ) > 0 ):
			self.ResetState()
			ret_dir = gameboard.TurnLeft( ret_dir )

		# Avoid immediate death by turning left
		for i in range( 4 ):
			if gameboard.GetRelativePixel( position, ret_dir, 1, 0 ) == 0:
				break
			self.ResetState()
			ret_dir = gameboard.TurnLeft( ret_dir )

		return ret_dir

