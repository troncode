import math

from troncode_values import *
from BasicGameBoard import BasicGameBoard

STATE_START        = 0  # Start
STATE_CUT1         = 1  # First part of cutoff step
STATE_TURN         = 2  # Turning around
STATE_CUT2         = 3  # Second part of cutoff step
STATE_NORMAL_LEFT  = 4  # Normal wallfollower behaviour
STATE_NORMAL_RIGHT = 5  # Reversed wallfollower behaviour
STATE_KILL_OUT     = 6  # We've noticed an opponent - move out to capture
STATE_KILL_AROUND  = 7  # Move along towards opponent to capture
STATE_KILL_DOWN    = 8  # Cut off behind opponent

ORIENTATION_UNKNOWN    = -1
ORIENTATION_VERTICAL   = 0
ORIENTATION_HORIZONTAL = 1

BOARD_SIZE = 200
ACCEPTABLE_SPACE = 90

TURN_AROUND_SIZE = 2000
KILL_OUT_PIXELS = 5

class SelfishGitPlayer( object ):
	"""Makes a nice space for itself with as few others in it as possible,
	then follows walls, turning around from time to time,
	and killing anyone it finds following walls by drawing a little box around them.
	by Andy Balaam"""

	def __init__( self ):
		self._state = STATE_START
		self.orientation = ORIENTATION_UNKNOWN
		self.final_dir = -1000
		self.turn_around_time = TURN_AROUND_SIZE
		self.kill_out_timer = KILL_OUT_PIXELS
		self.around_dir = -1000
		self.down_dir = -1000
		self.previous_normal_state = STATE_NORMAL_LEFT
		self.killing_player = -1000

	def GetColour( self ):
		return ( 185, 255, 185 )

	def GetName():
		return "Selfish Git"
	GetName = staticmethod( GetName )

	def GetShortLongDir( self, coord, pasthalf_dir, lessthanhalf_dir ):
		if coord > ( 200 - coord ):
			return pasthalf_dir
		else:
			return lessthanhalf_dir

	def GetLongestVerticalDir( self, y_pos ):
		return self.GetShortLongDir( y_pos, DIR_UP, DIR_DOWN )

	def GetShortestHorizontalDir( self, x_pos ):
		return self.GetShortLongDir( x_pos, DIR_RIGHT, DIR_LEFT )

	def GetLongestHorizontalDir( self, x_pos ):
		return self.GetShortLongDir( x_pos, DIR_LEFT, DIR_RIGHT )

	def GetNumOpponents( self, position, other_positions, direction ):
		ret = 0
		(my_x, my_y) = position
		for ((x,y), unused_dir) in other_positions:
			if direction == DIR_LEFT:
				if my_x >= x:
					ret += 1
			elif direction == DIR_RIGHT:
				if my_x <= x:
					ret += 1
			elif direction == DIR_UP:
				if my_y >= y:
					ret += 1
			elif direction == DIR_DOWN:
				if my_y <= y:
					ret += 1
		return ret

	def GetAcceptableDirs( self, position ):
		# Find all the cuts that give us > 40% of the game board
		space = {}
		space[DIR_LEFT] = position[0]
		space[DIR_RIGHT] = BOARD_SIZE - position[0]
		space[DIR_UP] = position[1]
		space[DIR_DOWN] = BOARD_SIZE - position[1]

		ret = []
		for dr in ( DIR_LEFT, DIR_RIGHT, DIR_UP, DIR_DOWN ):
			if space[dr] > ACCEPTABLE_SPACE:
				ret.append( dr )

		return ret

	def GetMostSpaceDir( self, position, dirs ):
		(x, y) = position
		max_space = -1000
		max_space_dir = -1000
		for dr in dirs:
			if dr == DIR_LEFT:
				space = x
			elif dr == DIR_RIGHT:
				space = BOARD_SIZE - x
			elif dr == DIR_UP:
				space = y
			elif dr == DIR_DOWN:
				space = BOARD_SIZE - y
			if space > max_space:
				max_space_dir = dr
				max_space = space
		return max_space_dir

	def GetBestDir( self, position, other_positions, acceptable_dirs ):
		min_num_opp = 1000
		dirs_min = []
		for direction in acceptable_dirs:
			num_opp = self.GetNumOpponents( position, other_positions, direction )
			if num_opp < min_num_opp:
				min_num_opp = num_opp
				dirs_min = [ direction ]
			elif num_opp == min_num_opp:
				dirs_min.append( direction )

		if len( dirs_min ) == 1:
			return dirs_min[0]
		else:
			return self.GetMostSpaceDir( position, dirs_min )

	def IsVertical( self, direction ):
		return ( direction == DIR_UP or direction == DIR_DOWN )

	def FindCut1DirFromBestDir( self, position, best_dir ):
		"""best_dir is the part of the board we want to capture - turn to start cutting it off"""

		# TODO: examine player positions to decide which side to cut off first.

		(x, y) = position
		if self.IsVertical( best_dir ):
			return self.GetLongestHorizontalDir( x )
		else:
			return self.GetLongestVerticalDir( y )

	def FindCut1Dir( self, position, gameboard ):
		ret_dir = DIR_LEFT

		other_positions = gameboard.GetPlayerPositions( position )

		acceptable_dirs = self.GetAcceptableDirs( position )

		best_dir = self.GetBestDir( position, other_positions, acceptable_dirs )

		self.final_dir = best_dir
		return self.FindCut1DirFromBestDir( position, best_dir )

	def FindFinalDir( self, position ):
		return self.final_dir

	def AnyPixelsBetween( self, my_pos, other_pos, gameboard ):
		off_x = other_pos[0] - my_pos[0]
		off_y = other_pos[1] - my_pos[1]

		max_dist = max( abs( off_x ), abs( off_y ), 1 )

		inc_x = float( off_x ) / float( max_dist )
		inc_y = float( off_y ) / float( max_dist )

		float_x = float( my_pos[0] )
		float_y = float( my_pos[1] )
		for i in range( max_dist ):
			float_x += inc_x
			float_y += inc_y
			x = math.floor( float_x )
			y = math.floor( float_y )
			if ( (x,y) != my_pos and (x,y) != other_pos and
					gameboard.GetAbsolutePixel( (x, y) ) != 0 ):
				return True

		return False

	def OppositeDir( self, direction, gameboard ):
		ret_dir = gameboard.TurnLeft( direction )
		ret_dir = gameboard.TurnLeft( ret_dir )
		return ret_dir

	def WithinKillRange( self, my_coord, other_coord ):
		return ( abs( my_coord - other_coord ) < 3 )

	def KillAble( self, position, other_position, other_dir, gameboard, ret_ckm ):
		( my_x, my_y ) = position
		( x, y ) = other_position

		if self.WithinKillRange( my_x, x ):
			if ( y <= my_y and other_dir == DIR_DOWN and
					not self.AnyPixelsBetween( position, other_position, gameboard ) ):
				ret_ckm.around_dir = DIR_UP
				return True
			if ( y >= my_y and other_dir == DIR_UP and
					not self.AnyPixelsBetween( position, other_position, gameboard ) ):
				ret_ckm.around_dir = DIR_DOWN
				return True
		if self.WithinKillRange( my_y, y ):
			if ( x <= my_x and other_dir == DIR_RIGHT and
					not self.AnyPixelsBetween( position, other_position, gameboard ) ):
				ret_ckm.around_dir = DIR_LEFT
				return True
			if ( x >= my_x and other_dir == DIR_LEFT and
					not self.AnyPixelsBetween( position, other_position, gameboard ) ):
				ret_ckm.around_dir = DIR_RIGHT
				return True
		return False

	def CheckKillMode( self, position, gameboard ):
		# TODO: check you are joined by a pixel line to the opponent?

		class CKM:
			def __init__( self ):
				self.can_kill = False
				self.state = None
				self.out_dir = None
				self.around_dir = None
				self.down_dir = None
				self.killing_player = None

		ret = CKM()

		other_positions = gameboard.GetPlayerPositions( position )

		player_num = 0
		for (other_position, other_dir) in other_positions:
			if self.KillAble( position, other_position, other_dir, gameboard, ret ):
				ret.can_kill = True
				ret.state = STATE_KILL_OUT
				ret.out_dir = gameboard.TurnLeft( other_dir )
				if gameboard.GetRelativePixel( position, ret.out_dir, 1, 0 ) > 0:
					ret.out_dir = self.OppositeDir( ret.out_dir, gameboard )
				ret.down_dir = self.OppositeDir( ret.out_dir, gameboard )
				ret.killing_player = player_num
				break
			player_num += 1

		return ret

	def KillAroundPast( self, position, other_position, kill_around_dir ):
		if ( ( kill_around_dir == DIR_LEFT  and position[0] < other_position[0] ) or
			 ( kill_around_dir == DIR_RIGHT and position[0] > other_position[0] ) or
			 ( kill_around_dir == DIR_UP    and position[1] < other_position[1] ) or
			 ( kill_around_dir == DIR_DOWN  and position[1] > other_position[1] ) ):
			return True
		else:
			return False

	def State2Str( self, state ):
		if state == STATE_START:
			return "STATE_START"
		elif state == STATE_CUT1:
			  return "STATE_CUT1"
		elif state == STATE_TURN:
			  return "STATE_TURN"
		elif state == STATE_CUT2:
			  return "STATE_CUT2"
		elif state == STATE_NORMAL_LEFT:
			  return "STATE_NORMAL_LEFT"
		elif state == STATE_NORMAL_RIGHT:
			  return "STATE_NORMAL_RIGHT"
		elif state == STATE_KILL_OUT:
			  return "STATE_KILL_OUT"
		elif state == STATE_KILL_AROUND:
			  return "STATE_KILL_AROUND"
		elif state == STATE_KILL_DOWN:
			  return "STATE_KILL_DOWN"

	def SetState( self, state ):
		self._state = state
		if state in ( STATE_NORMAL_LEFT, STATE_NORMAL_RIGHT ):
			self.previous_normal_state = state
		#print self.State2Str( state )

	def GetDir( self, position, direction, gameboard ):
		ret_dir = direction
		if self._state == STATE_START:
			ret_dir = self.FindCut1Dir( position, gameboard )
			self.SetState( STATE_CUT1 )

		elif self._state == STATE_CUT1:
			# Turn when we hit a wall
			if gameboard.GetRelativePixel( position, ret_dir, 1, 0 ) != 0:
				ret_dir = gameboard.TurnLeft( ret_dir )
				self.SetState( STATE_TURN )

		elif self._state == STATE_TURN:
			# Keep turning
			ret_dir = gameboard.TurnLeft( ret_dir )
			self.SetState( STATE_CUT2 )

		elif self._state == STATE_CUT2:
			if gameboard.GetRelativePixel( position, ret_dir, 1, 0 ) != 0:
				ret_dir = self.FindFinalDir( position )
				self.SetState( STATE_NORMAL_LEFT )

		elif self._state == STATE_KILL_OUT:
			self.kill_out_timer -= 1
			if self.kill_out_timer < 0:
				self.kill_out_timer = KILL_OUT_PIXELS
				self.SetState( STATE_KILL_AROUND )
		elif self._state == STATE_KILL_AROUND:
			other_position = gameboard.GetPlayerPositions( position )[ self.killing_player ][0]
			if self.KillAroundPast( position, other_position, self.kill_around_dir ):
				self.SetState( STATE_KILL_DOWN )
			else:
				ret_dir = self.kill_around_dir
		elif self._state == STATE_KILL_DOWN:
			ret_dir = self.kill_down_dir
		else:
			ckm = self.CheckKillMode( position, gameboard )
			if ckm.can_kill:
				other_position = gameboard.GetPlayerPositions( position )[0][0]
				#print "kill (%d,%d) (%d, %d)" % ( position[0], position[1], other_position[0], other_position[1] )
				self.SetState( ckm.state )
				self.kill_around_dir = ckm.around_dir
				self.kill_down_dir = ckm.down_dir
				self.killing_player = ckm.killing_player
				ret_dir = ckm.out_dir
			else:
				if self._state == STATE_NORMAL_LEFT:
					if gameboard.GetRelativePixel( position, ret_dir, 0, -1 ) == 0:
						# Turn left if we can
						ret_dir = gameboard.TurnLeft( ret_dir )
					else:
						self.turn_around_time -= 1
						if self.turn_around_time < 0:
							self.turn_around_time = TURN_AROUND_SIZE
							self.SetState( STATE_NORMAL_RIGHT )
				elif self._state == STATE_NORMAL_RIGHT:
					if gameboard.GetRelativePixel( position, ret_dir, 0, 1 ) == 0:
						ret_dir = gameboard.TurnRight( ret_dir )
					else:
						self.turn_around_time -= 1
						if self.turn_around_time < 0:
							self.turn_around_time = TURN_AROUND_SIZE
							self.SetState( STATE_NORMAL_LEFT )
				else:
					print "SelfishGit is in an extremely strange state.  The Master Control Program has been informed."
					self.SetState( STATE_NORMAL_LEFT )

		# Don't enter tunnels of width 1
		if ( gameboard.GetRelativePixel( position, ret_dir, 1, 1 ) > 0 and
			 gameboard.GetRelativePixel( position, ret_dir, 1, -1 ) > 0 ):
			ret_dir = gameboard.TurnRight( ret_dir )
			# If we're killing, favour the "around" direction to get us out of any trouble
			if self._state in ( STATE_KILL_OUT, STATE_KILL_OUT, STATE_KILL_DOWN ):
				ret_dir = self.kill_around_dir
			if self._state not in ( STATE_NORMAL_LEFT, STATE_NORMAL_RIGHT ):
				self.SetState( self.previous_normal_state )

		# If we're killing, favour the "around" direction to get us out of any trouble
		if ( self._state in ( STATE_KILL_OUT, STATE_KILL_OUT, STATE_KILL_DOWN ) and
			gameboard.GetRelativePixel( position, ret_dir, 1, 0 ) != 0 ):
				ret_dir = self.kill_around_dir
				self.SetState( self.previous_normal_state )

		# Avoid immediate death by turning right
		for i in range( 4 ):
			if gameboard.GetRelativePixel( position, ret_dir, 1, 0 ) == 0:
				break
				if self._state not in ( STATE_NORMAL_LEFT, STATE_NORMAL_RIGHT ):
					self.SetState( self.previous_normal_state )
			ret_dir = gameboard.TurnRight( ret_dir )

		return ret_dir

# ------ TESTS -------

def test_GetAcceptableDirs():
	player = SelfishGitPlayer()

	drs = player.GetAcceptableDirs( (100, 100) )
	assert( DIR_UP in drs )
	assert( DIR_DOWN in drs )
	assert( DIR_LEFT in drs )
	assert( DIR_RIGHT in drs )

	drs = player.GetAcceptableDirs( (10, 100) )
	assert( DIR_UP in drs )
	assert( DIR_DOWN in drs )
	assert( DIR_LEFT not in drs )
	assert( DIR_RIGHT in drs )

	drs = player.GetAcceptableDirs( (190, 100) )
	assert( DIR_UP in drs )
	assert( DIR_DOWN in drs )
	assert( DIR_LEFT in drs )
	assert( DIR_RIGHT not in drs )

	drs = player.GetAcceptableDirs( (10, 10) )
	assert( DIR_UP not in drs )
	assert( DIR_DOWN in drs )
	assert( DIR_LEFT not in drs )
	assert( DIR_RIGHT in drs )

	drs = player.GetAcceptableDirs( (100, 190) )
	assert( DIR_UP in drs )
	assert( DIR_DOWN not in drs )
	assert( DIR_LEFT in drs )
	assert( DIR_RIGHT in drs )

def test_GetNumOpponents():
	player = SelfishGitPlayer()

	# 2 to the top-left
	position = (100, 100)
	other_positions = [ ((10, 10), 0 ), ((15, 15), 0 ) ]
	assert( player.GetNumOpponents( position, other_positions, DIR_LEFT )  == 2 )
	assert( player.GetNumOpponents( position, other_positions, DIR_UP )    == 2 )
	assert( player.GetNumOpponents( position, other_positions, DIR_RIGHT ) == 0 )
	assert( player.GetNumOpponents( position, other_positions, DIR_DOWN )  == 0 )

	# 1 in each corner
	position = (30, 30)
	other_positions = [ ((10, 10), 0 ), ((190, 190), 0 ), ((190,10), 0), ((10, 190), 0) ]
	assert( player.GetNumOpponents( position, other_positions, DIR_LEFT )  == 2 )
	assert( player.GetNumOpponents( position, other_positions, DIR_UP )    == 2 )
	assert( player.GetNumOpponents( position, other_positions, DIR_RIGHT ) == 2 )
	assert( player.GetNumOpponents( position, other_positions, DIR_DOWN )  == 2 )

	# 2 to our immediate right
	position = (70, 70)
	other_positions = [ ((80, 70), 0 ) ]
	assert( player.GetNumOpponents( position, other_positions, DIR_LEFT )  == 0 )
	assert( player.GetNumOpponents( position, other_positions, DIR_UP )    == 1 )
	assert( player.GetNumOpponents( position, other_positions, DIR_RIGHT ) == 1 )
	assert( player.GetNumOpponents( position, other_positions, DIR_DOWN )  == 1 )

def test_GetMostSpaceDir():
	player = SelfishGitPlayer()

	assert( player.GetMostSpaceDir( (150, 75), [ DIR_UP, DIR_DOWN                      ] ) == DIR_DOWN )
	assert( player.GetMostSpaceDir( (150, 75), [ DIR_UP, DIR_DOWN, DIR_LEFT            ] ) == DIR_LEFT )
	assert( player.GetMostSpaceDir( (50, 75),  [ DIR_UP, DIR_DOWN, DIR_LEFT, DIR_RIGHT ] ) == DIR_RIGHT )
	assert( player.GetMostSpaceDir( (50, 125), [ DIR_UP,           DIR_LEFT            ] ) == DIR_UP )

def test_GetBestDir():
	player = SelfishGitPlayer()

	# Someone to our right, lots of room to the left
	acceptable_dirs = ( DIR_UP, DIR_DOWN, DIR_LEFT, DIR_RIGHT )
	position = (116, 114)
	other_positions = [ ((140, 140), 0) ]
	assert( player.GetBestDir( position, other_positions, acceptable_dirs ) == DIR_LEFT )

	# Someone above, lots of room below
	acceptable_dirs = ( DIR_UP, DIR_DOWN, DIR_LEFT, DIR_RIGHT )
	position = (114, 116)
	other_positions = [ ((140, 140), 0) ]
	assert( player.GetBestDir( position, other_positions, acceptable_dirs ) == DIR_UP )

	# People above, and to right - more room to left than down
	acceptable_dirs = ( DIR_UP, DIR_DOWN, DIR_LEFT, DIR_RIGHT )
	position = (115, 100)
	other_positions = [ ((140, 10), 0) ]
	assert( player.GetBestDir( position, other_positions, acceptable_dirs ) == DIR_LEFT )

	# Can only go up or left - choose up because other player is to left
	acceptable_dirs = ( DIR_UP, DIR_LEFT )
	position = (180, 180)
	other_positions = [ ((140, 190), 0) ]
	assert( player.GetBestDir( position, other_positions, acceptable_dirs ) == DIR_UP )

def test_IsVertical():
	player = SelfishGitPlayer()

	assert( player.IsVertical( DIR_UP ) )
	assert( player.IsVertical( DIR_DOWN ) )
	assert( not player.IsVertical( DIR_LEFT ) )
	assert( not player.IsVertical( DIR_RIGHT ) )

def test_FindCut1DirFromBestDir():
	player = SelfishGitPlayer()

	assert( player.FindCut1DirFromBestDir( (20,50), DIR_UP ) == DIR_RIGHT )
	assert( player.FindCut1DirFromBestDir( (20,50), DIR_LEFT ) == DIR_DOWN )
	assert( player.FindCut1DirFromBestDir( (120,50), DIR_UP ) == DIR_LEFT )
	assert( player.FindCut1DirFromBestDir( (120,50), DIR_LEFT ) == DIR_DOWN )
	assert( player.FindCut1DirFromBestDir( (120,150), DIR_LEFT ) == DIR_UP )

class FakeGameBoard( BasicGameBoard ):

	def __init__( self, other_positions, pixels ):
		self.other_positions = other_positions
		self.pixels = pixels

	def GetAbsolutePixel( self, pos ):
		if pos in self.pixels:
			retval = 1
		else:
			retval = 0

		return retval

	def GetPlayerPositions( self, pos_to_exclude ):
		return self.other_positions

	def GetArenaSize( self ):
		return (200, 200)

def test_WithinKillRange():
	player = SelfishGitPlayer()

	assert( player.WithinKillRange( 20, 20 ) )
	assert( player.WithinKillRange( 20, 21 ) )
	assert( not player.WithinKillRange( 20, 51 ) )

def test_CheckKillMode():
	player = SelfishGitPlayer()

	# One other player, and the kill is on
	position = ( 120, 199 )
	other_positions = [ ((50, 199), DIR_RIGHT ) ]
	gameboard = FakeGameBoard( other_positions, [] )
	ckm = player.CheckKillMode( position, gameboard )
	assert( ckm.can_kill )
	assert( ckm.state == STATE_KILL_OUT )
	assert( ckm.out_dir == DIR_UP )
	assert( ckm.around_dir == DIR_LEFT )
	assert( ckm.down_dir == DIR_DOWN )
	assert( ckm.killing_player == 0 )

	position = ( 120, 0 )
	other_positions = [ ((110, 0), DIR_RIGHT ) ]
	gameboard = FakeGameBoard( other_positions, [] )
	ckm = player.CheckKillMode( position, gameboard )
	assert( ckm.can_kill )
	assert( ckm.state == STATE_KILL_OUT )
	assert( ckm.out_dir == DIR_DOWN )
	assert( ckm.around_dir == DIR_LEFT )
	assert( ckm.down_dir == DIR_UP )
	assert( ckm.killing_player == 0 )

	position = ( 0, 12 )
	other_positions = [ ((0, 50), DIR_UP ) ]
	gameboard = FakeGameBoard( other_positions, [] )
	ckm = player.CheckKillMode( position, gameboard )
	assert( ckm.can_kill )
	assert( ckm.state == STATE_KILL_OUT )
	assert( ckm.out_dir == DIR_RIGHT )
	assert( ckm.around_dir == DIR_DOWN )
	assert( ckm.down_dir == DIR_LEFT )
	assert( ckm.killing_player == 0 )

	# One other player, and the kill is not on because they're going the wrong way
	position = ( 120, 199 )
	other_positions = [ ((50, 199), DIR_LEFT ) ]
	gameboard = FakeGameBoard( other_positions, [] )
	ckm = player.CheckKillMode( position, gameboard )
	assert( not ckm.can_kill )

	position = ( 199, 112 )
	other_positions = [ ((100, 100), DIR_DOWN ) ]
	gameboard = FakeGameBoard( other_positions, [] )
	ckm = player.CheckKillMode( position, gameboard )
	assert( not ckm.can_kill )

	# 2 players, and the kill is on
	position = ( 120, 199 )
	other_positions = [ ((50, 100), DIR_UP), ((50, 199), DIR_RIGHT ) ]
	gameboard = FakeGameBoard( other_positions, [] )
	ckm = player.CheckKillMode( position, gameboard )
	assert( ckm.can_kill )
	assert( ckm.state == STATE_KILL_OUT )
	assert( ckm.out_dir == DIR_UP )
	assert( ckm.around_dir == DIR_LEFT )
	assert( ckm.down_dir == DIR_DOWN )
	assert( ckm.killing_player == 1 )

	# One other player, no kill because there is a wall in the way
	position = ( 120, 199 )
	other_positions = [ ((50, 199), DIR_RIGHT ) ]
	occupied_pixels = [ (100, 199), (100, 198), (100, 197), (100, 196), (100, 195),
		                (100, 194), (100, 193), (100, 192), (100, 191), (100, 190) ]
	gameboard = FakeGameBoard( other_positions, occupied_pixels )
	ckm = player.CheckKillMode( position, gameboard )
	assert( not ckm.can_kill )

def test_AnyPixelsBetween():
	player = SelfishGitPlayer()

	gameboard = FakeGameBoard( [], [ (15, 120)] )
	assert( player.AnyPixelsBetween( (10, 10), (20, 10), gameboard ) == False )
	assert( player.AnyPixelsBetween( (10, 10), (10, 20), gameboard ) == False )
	assert( player.AnyPixelsBetween( (10, 120), (20, 120), gameboard ) == True )
	assert( player.AnyPixelsBetween( (15, 100), (15, 130), gameboard ) == True )

	gameboard = FakeGameBoard( [], [
		(15, 110),
		(15, 111),
		(15, 112),
		(15, 113),
		(15, 114),
		(15, 115),
		(15, 116),
		(15, 117),
		(15, 118),
		(15, 119),
		(15, 120),
		] )
	assert( player.AnyPixelsBetween( (10, 111), (20, 119), gameboard ) == True )

def test_OppositeDir():
	player = SelfishGitPlayer()
	gameboard = FakeGameBoard( [], [] )

	assert( player.OppositeDir( DIR_UP,    gameboard ) == DIR_DOWN )
	assert( player.OppositeDir( DIR_LEFT,  gameboard ) == DIR_RIGHT )
	assert( player.OppositeDir( DIR_DOWN,  gameboard ) == DIR_UP )
	assert( player.OppositeDir( DIR_RIGHT, gameboard ) == DIR_LEFT )

def test_KillAroundPast():
	player = SelfishGitPlayer()

	assert( player.KillAroundPast( ( 50, 5 ), ( 51, 0 ), DIR_LEFT ) == True )
	assert( player.KillAroundPast( ( 53, 5 ), ( 48, 0 ), DIR_LEFT ) == False )

	assert( player.KillAroundPast( ( 50, 195 ), ( 51, 199 ), DIR_RIGHT ) == False )
	assert( player.KillAroundPast( ( 53, 195 ), ( 48, 199 ), DIR_RIGHT ) == True )

	assert( player.KillAroundPast( ( 5, 195 ), ( 0, 199 ), DIR_UP ) == True )
	assert( player.KillAroundPast( ( 5, 195 ), ( 0, 190 ), DIR_UP ) == False )

	assert( player.KillAroundPast( ( 5, 195 ), ( 0, 199 ), DIR_DOWN ) == False )
	assert( player.KillAroundPast( ( 5, 195 ), ( 0, 190 ), DIR_DOWN ) == True )

def run_tests():
	test_GetAcceptableDirs()
	test_GetNumOpponents()
	test_GetMostSpaceDir()
	test_GetBestDir()
	test_IsVertical()
	test_FindCut1DirFromBestDir()
	test_AnyPixelsBetween()
	test_WithinKillRange()
	test_CheckKillMode()
	test_OppositeDir()
	test_KillAroundPast()
	print "Selfish Git tests passed."


