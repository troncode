from troncode_values import *

class WallFollowerPlayer( object ):
	"""Sticks as close to the wall as possible."""

	def __init__( self ):
		pass

	def GetColour( self ):
		return ( 255, 255, 253 )

	def GetName():
		return "WallFollower"
	GetName = staticmethod( GetName )

	def GetDir( self, position, direction, gameboard ):
		"""Stick as close as possible to any wall you find."""

		ret_dir = direction

		# Turn left if we can
		if gameboard.GetRelativePixel( position, direction, 0, -1 ) == 0:
			ret_dir = gameboard.TurnLeft( ret_dir )

		# Don't enter tunnels of width 1
		if ( gameboard.GetRelativePixel( position, ret_dir, 1, 1 ) > 0 and
			 gameboard.GetRelativePixel( position, ret_dir, 1, -1 ) > 0 ):
			ret_dir = gameboard.TurnRight( ret_dir )

		# Avoid immediate death by turning right
		for i in range( 4 ):
			if gameboard.GetRelativePixel( position, ret_dir, 1, 0 ) == 0:
				break
			ret_dir = gameboard.TurnRight( ret_dir )

		return ret_dir
