from AbstractGameBoard import AbstractGameBoard
from troncode_values import *

class BasicGameBoard( AbstractGameBoard ):
	"""Inherit from this if you want to make a fake GameBoard
	for testing.  It provides all the generic services of a
	GameBoard, but allows you to override the methods that
	provide information about the pixels and the players."""

	def GetAbsolutePixel( self, (x, y) ):
		raise Exception( "Not implemented" )

	def GetArenaSize( self ):
		raise Exception( "Not implemented" )

	def GetPlayerPositions( self, pos_to_exclude = None ):
		raise Exception( "Not implemented" )

	def GetRelativePixel( self, start_pos, facing_dir,
		offset_fwd, offset_right ):

		if facing_dir == DIR_UP:
			found_pos = ( start_pos[0] + offset_right,
						  start_pos[1] - offset_fwd )
		elif facing_dir == DIR_RIGHT:
			found_pos = ( start_pos[0] + offset_fwd,
						  start_pos[1] + offset_right )
		elif facing_dir == DIR_DOWN:
			found_pos = ( start_pos[0] - offset_right,
						  start_pos[1] + offset_fwd )
		elif facing_dir == DIR_LEFT:
			found_pos = ( start_pos[0] - offset_fwd,
						  start_pos[1] - offset_right )

		if ( found_pos[0] < 0 or
			 found_pos[0] >= self.GetArenaSize()[0] or
			 found_pos[1] < 0 or
			 found_pos[1] >= self.GetArenaSize()[1] ):
			retval = 100
		else:
			retval = self.GetAbsolutePixel( found_pos )

		return retval

	def TurnRight( self, direction ):
		direction += 1
		if direction > DIR_LEFT:
			direction = DIR_UP
		return direction

	def TurnLeft( self, direction ):
		direction -= 1
		if direction < DIR_UP:
			direction = DIR_LEFT
		return direction

